package com.example;

import java.util.Scanner;

import com.example.dto.Assets;
import com.example.dto.Months;
import com.example.dto.Sip;
import com.example.dto.UserPortfolio;
import com.example.factory.ServiceFactory;
import com.example.services.PortfolioServices;

public class App 
{
    public static void main( String[] args )
    {

       Scanner sc = new Scanner(System.in);
       String []inputs = new String[5]; 
       PortfolioServices portfolioServices = getPortfolio();
       UserPortfolio userPortfolio = new UserPortfolio();
       Assets userAsset;
       Sip sip = new Sip();

       do{

        inputs = sc.nextLine().split("\\s");
         
            if(inputs[0].trim().equals("ALLOCATE")) {

              userAsset = portfolioServices.calculateAllocate(inputs);
              userPortfolio.getPortfolio().put(Months.JANUARY,userAsset);
              
            } else if(inputs[0].trim().equals("SIP")){

                sip = new Sip(inputs);

            } else if(inputs[0].equals("CHANGE")){
                
                if(!inputs[4].equals("JANUARY")) {
                 
                 portfolioServices.calculateSIP(inputs,sip,userPortfolio);
                
                }
                portfolioServices.calculteChange(inputs, sip, userPortfolio);

            } else if(inputs[0].equals("BALANCE")){

                portfolioServices.calculateBalanceMonth(userPortfolio,Months.valueOf(inputs[1]));

            } else if(inputs[0].equals("REBALANCE")){

                if(userPortfolio.getPortfolio().size() > 5) {
                    portfolioServices.calculateRebalance(userPortfolio);
                } else {
                    System.out.println("\nCANNOT_REBALANCE");
                }
            }   
       }while(sc.hasNextLine());

       sc.close();
    }

    public static PortfolioServices getPortfolio() {
        return ServiceFactory.getService();
    }
}

