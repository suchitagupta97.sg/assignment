package com.example.dto;

public class Assets {

    private long equity;
    private long debt;
    private long gold;
    private double equityPercent;
    private double debtPercent;
    private double goldPercent;

    public Assets(){
        
    }

    public Assets(long equity, long debt, long gold){
        this.equity = equity;
        this.debt = debt;
        this.gold = gold;
        long total = equity + debt + gold;
        calculatePercent(total);
    }

    public void calculatePercent(long total) {
        equityPercent = (equity * 100)/ total;
        debtPercent = (debt * 100)/ total;
        goldPercent = (gold * 100)/ total;
    }

    public long getEquity() {
        return equity;
    }

    public void setEquity(long equity) {
        this.equity = equity;
    }

    public long getDebt() {
        return debt;
    }

    public void setDebt(long debt) {
        this.debt = debt;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public double getEquityPercent() {
        return equityPercent;
    }

    public void setEquityPercent(double equityPercent) {
        this.equityPercent = equityPercent;
    }

    public double getDebtPercent() {
        return debtPercent;
    }

    public void setDebtPercent(double debtPercent) {
        this.debtPercent = debtPercent;
    }

    public double getGoldPercent() {
        return goldPercent;
    }

    public void setGoldPercent(double goldPercent) {
        this.goldPercent = goldPercent;
    }
    
}