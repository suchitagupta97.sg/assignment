package com.example.dto;

public class Sip {

    public long equitySIP;
    public long debtSIP;
    public long goldSIP;


    public Sip(String []inputs){
        this.equitySIP = Long.parseLong(inputs[1]);
        this.debtSIP = Long.parseLong(inputs[2]);
        this.goldSIP = Long.parseLong(inputs[3]);
    }

    public Sip(){
        this.equitySIP = 0;
        this.debtSIP =0;
        this.goldSIP =0;
    }

    public long getEquitySIP() {
        return equitySIP;
    }

    public void setEquitySIP(long equitySIP) {
        this.equitySIP = equitySIP;
    }

    public long getDebtSIP() {
        return debtSIP;
    }

    public void setDebtSIP(long debtSIP) {
        this.debtSIP = debtSIP;
    }

    public long getGoldSIP() {
        return goldSIP;
    }

    public void setGoldSIP(long goldSIP) {
        this.goldSIP = goldSIP;
    }
    
}