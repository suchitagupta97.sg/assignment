package com.example.dto;

import java.util.LinkedHashMap;

public class UserPortfolio {

    private LinkedHashMap<Months,Assets> portfolio;

    public UserPortfolio(){
        portfolio = new LinkedHashMap<Months,Assets>();
    }

    public LinkedHashMap<Months, Assets> getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(LinkedHashMap<Months, Assets> portfolio) {
        this.portfolio = portfolio;
    }
    
}