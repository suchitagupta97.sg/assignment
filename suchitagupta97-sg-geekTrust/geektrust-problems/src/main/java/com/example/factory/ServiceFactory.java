package com.example.factory;

import com.example.services.PortfolioServices;
import com.example.services.PortfolioServicesImpl;

public class ServiceFactory {

    static PortfolioServices portfolioServices;

    public ServiceFactory(){

    }

    public static PortfolioServices getService(){

        if(portfolioServices == null){
            portfolioServices = new PortfolioServicesImpl();
            return portfolioServices;
        }
      return portfolioServices;  
    }
}