package com.example.services;

import com.example.dto.Assets;
import com.example.dto.Months;
import com.example.dto.Sip;
import com.example.dto.UserPortfolio;

public interface PortfolioServices {
    
    Assets calculateAllocate(String []inputs);
    void calculateSIP(String []inputs, Sip sip, UserPortfolio userPortfolio);
    void calculteChange(String []inputs, Sip sip, UserPortfolio userPortfolio);
    void calculateBalanceMonth(UserPortfolio userPortfolio ,Months month);
    void calculateRebalance(UserPortfolio userPortfolio);

}