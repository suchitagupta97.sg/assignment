package com.example.services;

import com.example.dto.Assets;
import com.example.dto.Months;
import com.example.dto.Sip;
import com.example.dto.UserPortfolio;

public class PortfolioServicesImpl implements PortfolioServices {

    @Override
    public Assets calculateAllocate(String []inputs) {

        return new Assets(Long.parseLong(inputs[1]), 
                    Long.parseLong(inputs[2]),
                    Long.parseLong(inputs[3]));
    }

    @Override
    public void calculateSIP(String []inputs, Sip sip, UserPortfolio userPortfolio) {
       
       // Months prevMonth = Months.values()[month.ordinal()-1]; 
        Months month = Months.valueOf(inputs[4]); 
        Months prevMonth = Months.values()[month.ordinal()-1];
        Assets prevAssets = userPortfolio.getPortfolio().get(prevMonth);
        Assets asset = new Assets();
        asset.setEquity(prevAssets.getEquity() + sip.getEquitySIP());
        asset.setDebt(prevAssets.getDebt() + sip.getDebtSIP());
        asset.setGold(prevAssets.getGold() + sip.getGoldSIP());
        userPortfolio.getPortfolio().put(month, asset);

    }

    @Override
    public void calculteChange(String []inputs, Sip sip, UserPortfolio userPortfolio) {
        
        Assets asset = userPortfolio.getPortfolio().get(Months.valueOf(inputs[4]));
        
        double equityPercent = Double.parseDouble(inputs[1].split("%")[0]);
        asset.setEquity((long)(asset.getEquity() * (100 + equityPercent) / 100));

        double debtPercent = Double.parseDouble(inputs[2].split("%")[0]);
        asset.setDebt((long)(asset.getDebt() * (100 + debtPercent) / 100));

        double goldPercent = Double.parseDouble(inputs[3].split("%")[0]);
        asset.setGold((long)(asset.getGold() * (100 + goldPercent) / 100));

        if(inputs[4].equals("JANUARY")){
            userPortfolio.getPortfolio().put(Months.JANUARY,asset);
        }

    }

    @Override
    public void calculateBalanceMonth(UserPortfolio userPortfolio ,Months month) {
        
        Assets currAssest = userPortfolio.getPortfolio().get(month);
        System.out.print("\n"+currAssest.getEquity() + " " 
                + currAssest.getDebt() + " " + currAssest.getGold());
    }

    @Override
    public void calculateRebalance(UserPortfolio userPortfolio) {
        
        long total;
        double equityPercent = userPortfolio.getPortfolio().get(Months.JANUARY)
            .getEquityPercent();
        
        double debtPercent = userPortfolio.getPortfolio().get(Months.JANUARY)
            .getDebtPercent();
                    
        double goldPercent = userPortfolio.getPortfolio().get(Months.JANUARY)
            .getGoldPercent();            


        if(userPortfolio.getPortfolio().containsKey(Months.DECEMBER)){
            total = userPortfolio.getPortfolio().get(Months.DECEMBER).getEquity()
              + userPortfolio.getPortfolio().get(Months.DECEMBER).getDebt()
              + userPortfolio.getPortfolio().get(Months.DECEMBER).getGold();
        } else {

            total = userPortfolio.getPortfolio().get(Months.JUNE).getEquity()
              + userPortfolio.getPortfolio().get(Months.JUNE).getDebt()
              + userPortfolio.getPortfolio().get(Months.JUNE).getGold();
        }

        long equityValue = (long)(total*equityPercent/100);
        long debtValue = (long)(total*debtPercent/100);
        long goldValue = (long)(total*goldPercent/100);

        System.out.println(equityValue+" "+debtValue+" "+goldValue);


    }

    
}