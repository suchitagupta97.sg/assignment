package com.example;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import com.example.dto.Assets;
import com.example.dto.Sip;
import com.example.dto.UserPortfolio;
import com.example.services.PortfolioServicesImpl;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

    String input = "";
    PortfolioServicesImpl portfolioServicesImpl;

    @Before
    public void initPortfolio(){
        portfolioServicesImpl = new PortfolioServicesImpl(); 
    }

    @Test
    public void checkAllocation() {

        input = "ALLOCATE 6000 3000 1000";
        Assets asset = portfolioServicesImpl.calculateAllocate(input.split("\\s"));
        assertNotNull(asset);
    }
    
    @Test
    public void checkSIP() {
 
        input = "SIP 2000 1000 500";
        Sip sip = new Sip(input.split("\\s"));
        assertNotNull(sip);
        assertTrue(sip.getGoldSIP()==500L);
        assertTrue(sip.getDebtSIP()==1000L);
        assertTrue(sip.getEquitySIP()==2000L);
    }

   

}
