package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.example.dto.Assets;
import com.example.factory.ServiceFactory;
import com.example.services.PortfolioServices;
import com.example.services.PortfolioServicesImpl;

import org.junit.Before;
import org.junit.Test;

public class ServiceTest {

  String input = "";
  PortfolioServices portfolioServicesImpl;

  @Before
  public void initPortfolio(){
      portfolioServicesImpl = new PortfolioServicesImpl(); 
  }

  @Test
  public void checkFactory(){

    PortfolioServices service1 = ServiceFactory.getService();
    PortfolioServices service2 = ServiceFactory.getService(); 
    PortfolioServices service3 = ServiceFactory.getService();
    PortfolioServices service4 = ServiceFactory.getService();  
    assertEquals(service1, service2);
    assertEquals(service3, service4);
    assertEquals(service1, service4);
  }

  @Test
  public void checkCalculations(){

    input = "ALLOCATE 6000 3000 1000";
    Assets userAsset = portfolioServicesImpl.calculateAllocate(input.split("\\s"));
    assertTrue(userAsset.getEquityPercent() == 60);
    assertTrue(userAsset.getDebtPercent() == 30);
    assertTrue(userAsset.getGoldPercent() == 10);
  }

}